﻿/** 
 * @file Mitch_HW.cs
 * 
 * This file is part of Mitch_API library.

 * Mitch_API is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Mitch_API is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Nome-Programma.If not, see<http://www.gnu.org/licenses/>.

 * Copyright (c) 2020 by 221e srl.
 */

namespace _221e.Mitch
{
    /**
    * @brief Mitch_HW class
    *
    * Hardware related communication protocol specifications
    *
    * @todo add some functionalities
    */
    public class Mitch_HW
    {
        /// <summary>Overall message length [Bytes]</summary>
        public const int COMM_MESSAGE_LEN = 20;

        /// <summary>Message Header of 4 Bytes length</summary>
        public const int COMM_MSG_HEADER_LEN_4 = 4;
        /// <summary>Message Header of 2 Bytes length</summary>
        public const int COMM_MSG_HEADER_LEN_2 = 2;

        /// <summary>Command, Payload maximum length [Bytes]</summary>
        public const int COMM_PAYLOAD_LEN_18 = 18;
        /// <summary>Response, Payload maximum length [Bytes]</summary>
        public const int COMM_PAYLOAD_LEN_16 = 16;

        /// <summary>Number of data channels (9 Degrees of Freedom, e.g. Gyr+Axl+Mag)</summary>
        public const int NUM_OF_CHANNELS_9DOF = 3;
        /// <summary>Number of data channels (Time Of Flight)</summary>
        public const int NUM_OF_CHANNELS_TOF = 2;

        /// <summary>
        /// Command statements
        /// </summary>
        public enum Command : byte
        {
            /// <summary>Acknowledge</summary>
            CMD_ACK = 0x00,
            /// <summary>Shutdown</summary>
            CMD_SHUTDOWN = 0x01,
            /// <summary>State [get/set]</summary>
            CMD_STATE = 0x02,
            /// <summary>Restart</summary>
            CMD_RESTART = 0x03,
            /// <summary>Cyclical Redundancy Checking [readonly]</summary>
            CMD_APP_CRC = 0x04,
            /// <summary>Firmware Upload [<b>bootloader mode only</b>]</summary>
            CMD_FW_UPLOAD = 0x05,
            /// <summary>Start Application</summary>
            CMD_START_APP = 0x06,
            /// <summary>Battery Charge [readonly]</summary>
            CMD_BATTERY_CHARGE = 0x07,
            /// <summary>Battery Voltage [readonly]</summary>
            CMD_BATTERY_VOLTAGE = 0x08,
            /// <summary>Device check up flag [readonly]</summary>
            CMD_CHECK_UP = 0x09,
            /// <summary>Installed Firmware Version [readonly]</summary>
            CMD_FW_VERSION = 0x0A,
            /// <summary>Current Time [get/set]</summary>
            CMD_TIME = 0x0B,
            /// <summary>Bluetooth Module Name [get/set]</summary>
            CMD_BLE_NAME = 0x0C,
            /// <summary>Hardware version [readonly]</summary>
            CMD_HW_VERSION = 0x0D,
            /// <summary>Device Identification code [readonly]</summary>
            CMD_DEVICE_ID = 0x0E,

            /// <summary>Memory state [readonly]</summary>
            CMD_MEM_CONTROL = 0x20,
            /// <summary>Get file information [readonly]</summary>
            CMD_MEM_FILE_INFO = 0x21,
            /// <summary>File download</summary>
            CMD_MEM_FILE_DOWNLOAD = 0x22,

            /// <summary>Clock drift [get/set]</summary>
            CMD_CLK_DRIFT = 0x30,
            /// <summary>Clock offset [get/set]</summary>
            CMD_CLK_OFFSET = 0x31,
            /// <summary>Enter time sync mode</summary>
            CMD_TIME_SYNC = 0x32,
            /// <summary>Exit time sync mode</summary>
            CMD_EXIT_TIME_SYNC = 0x33,

            /// <summary>Accelerometer + Gyroscope full scales [get/set]</summary>
            CMD_FS_AXL_GYRO = 0x40,
            /// <summary>Accelerometer full scale [get/set]</summary>
            CMD_FS_AXL = 0x41,
            /// <summary>Gyroscope full scale [get/set]</summary>
            CMD_FS_GYRO = 0x42,

            /// <summary>Automatic start</summary>
            CMD_AUTO_START = 0x43,

            /// <summary>Full scale of TOF#1 [get/set]</summary>
            CMD_FS_DS1 = 0x44,
            /// <summary>Full scale of TOF#2 [get/set]</summary>
            CMD_FS_DS2 = 0x45,
            /// <summary>Offset of TOF#1 [get/set]</summary>
            CMD_OFFSET_DS1 = 0x46,
            /// <summary>Offset of TOF#2 [get/set]</summary>
            CMD_OFFSET_DS2 = 0x47,

            /// <summary>Device calibration matrices [get/set]</summary>
            CMD_MATRIX_CALIBRATION = 0x48,

            /// <summary>Log button</summary>
            CMD_BTN_LOG = 0x50         
        }

        /// <summary>
        /// Acknowledge types
        /// </summary>
        public enum Acknowledge_Type : byte
        {
            /// <summary>Positive Acknowledge</summary>
            CMD_ACK_SUCCESS = 0x00,
            /// <summary>Negative Acknowledge</summary>
            CMD_ACK_ERROR = 0x01                            
        }

        /// <summary>
        /// System States
        /// </summary>
        public enum SystemState : byte
        {
            /// <summary>System state NULL</summary>
            SYS_NULL = 0x00,
            /// <summary>Startup state [<b>bootloader mode only</b>]</summary>
            SYS_BOOT_STARTUP = 0xF0,
            /// <summary>Idle state [<b>bootloader mode only</b>]</summary>
            SYS_BOOT_IDLE = 0xF1,
            /// <summary>Write state [<b>bootloader mode only</b>]</summary>
            SYS_BOOT_WRITE = 0xF2,
            /// <summary>Error state</summary>
            SYS_ERROR = 0xFF,
            /// <summary>Startup state</summary>
            SYS_STARTUP = 0x01,
            /// <summary>Idle state</summary>
            SYS_IDLE = 0x02,
            /// <summary>Standby</summary>
            SYS_STANDBY = 0x03,
            /// <summary>Log state</summary>
            SYS_LOG = 0x04,                                 
            /// <summary>Readout memory state</summary>
            SYS_READOUT = 0x05,
            /// <summary>Streaming state</summary>
            SYS_TX = 0xF8                                   
        }

        /// <summary>
        /// Restart modes
        /// </summary>
        public enum RestartMode : byte
        {
            /// <summary>Restart device in application mode</summary>
            RESTART_RESET = 0x00,
            /// <summary>Restart device in bootloader mode</summary>
            RESTART_BOOT_LOADER = 0x01            
        }

        /// <summary>
        /// Allowed acquisition types, operating in log mode
        /// </summary>
        public enum LogMode : byte
        {
            /// <summary>None</summary>
            LOG_MODE_NONE = 0x00,
            /// <summary>Inertial Measurement Unit</summary>
            LOG_MODE_IMU = 0x01,
            /// <summary>Inertial Measurement Unit + Pressure / Insole</summary>
            LOG_MODE_IMU_INSOLE = 0x02,
            /// <summary>All</summary>
            LOG_MODE_ALL = 0x03,
            /// <summary>Inertial Measurement Unit + Timestamp</summary>
            LOG_MODE_IMU_TIMESTAMP = 0x04,
            /// <summary>Inertial Measurement Unit + Pressure / Insole + Timestamp</summary>
            LOG_MODE_IMU_INSOLE_TIMPESTAMP = 0x05,
            /// <summary>All + Timestamp</summary>
            LOG_MODE_ALL_TIMESTAMP = 0x06
        }

        /// <summary>
        /// Allowed acquisition frequencies, operating in log mode
        /// </summary>
        public enum LogFrequency : byte
        {
            /// <summary>None</summary>
            LOG_FREQ_NONE = 0x00,
            /// <summary>25 Hz</summary>
            LOG_FREQ_25HZ = 0x01,
            /// <summary>50 Hz</summary>
            LOG_FREQ_50HZ = 0x02,
            /// <summary>100 Hz</summary>
            LOG_FREQ_100HZ = 0x04,
            /// <summary>200 Hz</summary>
            LOG_FREQ_200HZ = 0x08,
            /// <summary>500 Hz</summary>
            LOG_FREQ_500HZ = 0x14,
            /// <summary>1000 Hz</summary>
            LOG_FREQ_1000HZ = 0x28     
        }

        /// <summary>
        /// Packet dimensions (log)
        /// </summary>
        public enum LogPacketDimension : byte
        {
            /// <summary>None</summary>
            LOG_PACKET_DIM_NONE = 0,
            /// <summary>Inertial Measurement Unit</summary>
            LOG_PACKET_DIM_IMU = 18,
            /// <summary>Inertial Measurement Unit + Pressure / Insole</summary>
            LOG_PACKET_DIM_IMU_INSOLE = 42,
            /// <summary>All</summary>
            LOG_PACKET_DIM_ALL = 42,
            /// <summary>Inertial Measurement Unit + Timestamp</summary>
            LOG_PACKET_DIM_IMU_withTimestamp = 26,
            /// <summary>Inertial Measurement Unit + Pressure / Insole + Timestamp</summary>
            LOG_PACKET_DIM_IMU_INSOLE_withTimestamp = 50,
            /// <summary>All + Timestamp</summary>
            LOG_PACKET_DIM_ALL_withTimestamp = 52     
        }

        /// <summary>
        /// Allowed acquisition types, operating in streaming mode
        /// </summary>
        public enum StreamMode : byte
        {
            /// <summary>None</summary>
            STREAM_MODE_NONE = 0x00,
            /// <summary>Pressure</summary>
            STREAM_MODE_PRESSURE = 0x01,
            /// <summary>Gyroscope + Accelerometer + Time Of Flight</summary>
            STREAM_MODE_6DOF_TOF = 0x02,
            /// <summary>Time Of Flight</summary>
            STREAM_MODE_TOF = 0x03,
            /// <summary>Gyroscope + Accelerometer</summary>
            STREAM_MODE_6DOF = 0x04,
            /// <summary>Gyroscope + Accelerometer + Magnetometer</summary>
            STREAM_MODE_9DOF = 0x05,
            /// <summary>Gyroscope + Accelerometer + Quaternion</summary>
            STREAM_MODE_6DOFs_ORIENTATION = 0x06,
            /// <summary>Quaternion</summary>
            STREAM_MODE_ORIENTATION = 0x07	      
        }

        /// <summary>
        /// Allowed acquisition frequencies, operating in streaming mode
        /// </summary>
        public enum StreamFrequency : byte
        {
            /// <summary>None</summary>
            STREAM_FREQ_NONE = 0x00,
            /// <summary>5 Hz</summary>
            STREAM_FREQ_5Hz = 0x01,
            /// <summary>10 Hz</summary>
            STREAM_FREQ_10Hz = 0x02,
            /// <summary>25 Hz</summary>
            STREAM_FREQ_25Hz = 0x03,
            /// <summary>50 Hz</summary>
            STREAM_FREQ_50Hz = 0x04          
        }

        /// <summary>
        /// Packet dimensions (stream)
        /// </summary>
        public enum StreamPacketDimension : byte
        {
            /// <summary>None</summary>
            STREAM_PACKET_DIM_NONE = 0,
            /// <summary>Pressure</summary>
            STREAM_PACKET_DIM_PRESSURE = 18,
            /// <summary>Gyroscope + Accelerometer + Time Of Flight</summary>
            STREAM_PACKET_DIM_6DOF_TOF = 16,
            /// <summary>Time Of Flight</summary>
            STREAM_PACKET_DIM_TOF = 4,
            /// <summary>Gyroscope + Accelerometer</summary>
            STREAM_PACKET_DIM_6DOF = 14,
            /// <summary>Gyroscope + Accelerometer + Magnetometer</summary>
            STREAM_PACKET_DIM_9DOF = 18      
        }

        /// <summary>
        /// Memory slot selector
        /// </summary>
        public enum MemorySelection : byte
        {
            /// <summary>Memory slot selector #1</summary>
            MEMORY_ONE = 0x01,
            /// <summary>Memory slot selector #2</summary>
            MEMORY_TWO = 0x02                               
        }

        /// <summary>
        /// Memory erase type
        /// </summary>
        public enum MemoryErase_Type : byte
        {
            /// <summary>Memory slot selector #1</summary>
            ERASE_NONE = 0x00,
            /// <summary>Partial erase of the flash memory, from the beginning to the last sector occupied</summary>
            ERASE_PARTIAL = 0x01,
            /// <summary>Total erase of the memory slot #1</summary>
            ERASE_BULK1 = 0x02,                           
            /// <summary>Total erase of the memory slot #2</summary>
            ERASE_BULK2 = 0x03                              
        }

        /// <summary>
        /// Gyroscope full scale values [dps]
        /// </summary>
        public enum Gyroscope_FS : byte
        {
            /// <summary>None</summary>
            GYR_FS_NULL = 0xFF,
            /// <summary>245 dps</summary>
            GYR_FS_245_DPS = 0x00,
            /// <summary>500 dps</summary>
            GYR_FS_500_DPS = 0x04,
            /// <summary>1000 dps</summary>
            GYR_FS_1000_DPS = 0x08,
            /// <summary>2000 dps</summary>
            GYR_FS_2000_DPS = 0x0C
        }

        /// <summary>Gyroscope resolution with a FS of 245 dps</summary>
        public const float GYR_RESOLUTION_245dps = 8.75f;
        /// <summary>Gyroscope resolution with a FS of 500 dsp </summary>
        public const float GYR_RESOLUTION_500dps = 17.5f;
        /// <summary>Gyroscope resolution with a FS of  1000 dps </summary>
        public const float GYR_RESOLUTION_1000dps = 35f;
        /// <summary>Gyroscope resolution with a FS of  2000 dps </summary>
        public const float GYR_RESOLUTION_2000dps = 70f;

        /// <summary> 
        /// Accelerometer full scale values [g]
        /// </summary>
        public enum Accelerometer_FS : byte
        {
            /// <summary>None</summary>
            AXL_FS_NULL = 0xFF,
            /// <summary>2 g</summary>
            AXL_FS_2_g = 0x00,
            /// <summary>4 g </summary>
            AXL_FS_4_g = 0x08,
            /// <summary>8 g </summary>
            AXL_FS_8_g = 0x0C,
            /// <summary>16 g </summary>
            AXL_FS_16_g = 0x04
        }

        /// <summary>Accelerometer resolution with a FS of 2g</summary>
        public const float AXL_RESOLUTION_2g = 0.061f;
        /// <summary>Accelerometer resolution with a FS of 4g</summary>
        public const float AXL_RESOLUTION_4g = 0.122f;
        /// <summary>Accelerometer resolution with a FS of 8g</summary>
        public const float AXL_RESOLUTION_8g = 0.244f;
        /// <summary>Accelerometer resolution with a FS of 16g</summary>
        public const float AXL_RESOLUTION_16g = 0.488f;

        /// <summary>Magnetometer resolution</summary>
        public const float MAG_RESOLUTION = 1.5f;

        /// <summary> 
        /// Time Of Flight distance sensor full scales
        /// </summary>
        public enum TOF_FS : byte
        {
            /// <summary>None</summary>
            TOF_FS_NULL = 0x00,
            /// <summary>200 mm</summary>
            TOF_FS_200mm = 0x01,                           
            /// <summary>400 mm</summary>
            TOF_FS_400mm = 0x02,                           
            /// <summary>600 mm</summary>
            TOF_FS_600mm = 0x03                            
        };

        /// <summary> 
        /// Calibration matrices index
        /// </summary>
        public enum CalibMatrixType : byte
        {
            /// <summary>Accelerometer</summary>
            AXL_MATRIX = 0x00,
            /// <summary>Gyroscope</summary>
            GYR_MATRIX = 0x01,
            /// <summary>Magnetometer</summary>
            MAG_MATRIX = 0x02
        };
    }
}
